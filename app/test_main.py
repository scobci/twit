from fastapi.testclient import TestClient

from main import app

client = TestClient(app)


def test_read_main():
    response = client.get("/")
    assert response.status_code == 404


def test_read_not_main():
    response = client.get("/not_main")
    assert response.status_code == 404
